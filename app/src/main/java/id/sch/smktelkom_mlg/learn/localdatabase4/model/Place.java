package id.sch.smktelkom_mlg.learn.localdatabase4.model;

import io.realm.RealmObject;

/**
 * Created by hyuam on 12/10/2016.
 */

public class Place extends RealmObject
{
    //public Long id;
    public String judul;
    public String deskripsi;
    public String detail;
    public String lokasi;
    public String foto;
    
    public Place()
    {
    }
    
    public Place(String judul, String deskripsi, String detail, String lokasi,
                 String foto)
    {
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.detail = detail;
        this.lokasi = lokasi;
        this.foto = foto;
    }
    
}
